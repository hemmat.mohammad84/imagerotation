from django.http import HttpResponse
from .rotation import Rotation
import json




def data_prep(request):
    filename = request.POST.get('filename')
    vendor = request.POST.get('vendor')
    direction = request.POST.get('direction')
    imagePrefixPath= request.POST.get('imagePrefixPath')
    info = {'filename': filename, 'vendor': vendor, 'direction': direction,
            'imagePrefixPath': imagePrefixPath}
    resp = Rotation().load_images_from_azure(info)
    return HttpResponse(json.dumps(resp))


