from PIL import Image

class Thumbnail:


    def ratio(self):
        return self.image.size[1] / self.image.size[0]

    def modify(self, height):
        width = int(round(height * self.ratio(), 0))
        im = self.image.resize((width, height), resample=Image.ANTIALIAS)
        return im

    def thumbnail(self, data):
        try:
            self.data = data
            self.image = data['image_file']
            Tim = self.modify(100)
            self.data['image_file'] = Tim
            self.data['image_name'] = self.data['image_name'] + '_T'
            return {'state': True, 'output': self.data}
        except Exception as e:
           return {'state': False, 'output':'thumbnail failed {}'.format(str(e))}
