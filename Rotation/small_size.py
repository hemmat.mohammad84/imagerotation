from PIL import Image


class Small_size:

    def ratio(self):
        return self.image.size[1] / self.image.size[0]

    def modify(self, height):
        width = int(round(height * self.ratio(), 0))
        im = self.image.resize((width, height), resample=Image.ANTIALIAS)
        return im

    def small_size(self, data):
        try:
            self.data = data
            self.image = data['image_file']

            Sim = self.modify(400)
            self.data['image_file'] = Sim
            self.data['image_name'] = self.data['image_name'] + '_S'
            return {'state': True, 'output': self.data}
        except Exception as e:
                return {'state': False, 'output': 'small size failed {}'.format(str(e))}

