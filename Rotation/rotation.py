from .azure_connection import Azure
from PIL import Image
from .thumbnail import Thumbnail
from .small_size import Small_size
from multiprocessing.pool import ThreadPool
from multiprocessing import Pool
import json
class Rotation():




    def load_images_from_azure(self, data):
        try:
            self.azure_instance = Azure()
            self.data = data
            self.response = {'vendor': data['vendor'],
                             'image_name': data['filename'],
                             'error': []
                             }
            if self.data['imagePrefixPath'] and self.data['filename'] and self.data['direction']:
                format_name = '.jpg'
                images = []
                containar = self.data['imagePrefixPath'].split('/')[0]
                filename = self.data['filename']
                blob = {'blob': self.data['imagePrefixPath']
                                [self.data['imagePrefixPath'].index('/'):][1:]+'/'+filename + format_name,
                        'container': containar}



                finding_image_response = Azure().find_image(blob)

                if finding_image_response['state']:

                    image_o = {'image_file': finding_image_response['output'], 'image_name': self.data['filename'],
                               'path_name': self.data['imagePrefixPath']}
                    images.append(image_o)
                    data_for_creating_size = {'image_file': finding_image_response['output'], 'image_name': self.data['filename'],
                                              'path_name': self.data['imagePrefixPath']}
                    data_for_creating_size_ = {'image_file': finding_image_response['output'], 'image_name': self.data['filename'],
                                               'path_name': self.data['imagePrefixPath']}

                    image_t = Thumbnail().thumbnail(data_for_creating_size)
                    if image_t['state']:
                        images.append(image_t['output'])
                    else:
                        self.response['error'].append(image_t['output'])
                    image_s = Small_size().small_size(data_for_creating_size_)
                    if image_s['state']:
                        images.append(image_s['output'])
                    else:
                        self.response['error'].append(image_s['output'])

                    with Pool(3) as image_rotator:
                        rotated_images = image_rotator.map(self.rotate_, images)
                    images = []

                    for i in rotated_images:
                        if i['state']:
                            images.append(i['output'])


                    with ThreadPool(3) as image_uploader:
                        uploaded = image_uploader.map(Azure().upload, images)

                    for upload in uploaded:
                        if not upload['state']:
                            self.response['error'].append(upload['output'])
                else:
                    self.response['error'].append(finding_image_response['output'])
            return self.response


        except Exception as e:
            self.response['error'].append(
                {'state': False, 'output': 'load images failed {}'.format(str(e))})
            return self.response

    def rotate_(self, image):

        try:
            if self.data['direction'] == 'left':
                image['image_file'] = image['image_file'].transpose(Image.ROTATE_90)
            if self.data['direction'] == 'right':
                image['image_file'] = image['image_file'].transpose(Image.ROTATE_270)

            return {'state': True, 'output': image}
        except Exception as e:
           return {'state': False, 'output': image['name'] +
                                             'failed to rotate because {}'.format(str(e))}



