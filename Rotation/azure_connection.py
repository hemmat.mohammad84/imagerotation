from azure.storage.blob import BlockBlobService, PublicAccess, ContentSettings
from io import BytesIO
from PIL import Image






class Azure():





    def __init__(self):
        try:
            ACN = "rmimages2"
            ACK = "VwyVpLyDlhKEmhO8WsGNOxIj0pGna4tf+wlUe17EneNyACGyvXemKujA7dwypWZt9gFjMNwW4Vjm7jmVAJ1iRg=="
            self.block_blob_service = BlockBlobService(account_name=ACN, account_key=ACK)
        except Exception as e:
            return







    def find_image(self, data):
            try:


                if self.block_blob_service.exists(data['container'], data['blob']):
                    image = self.block_blob_service.get_blob_to_bytes(data['container'], data['blob'])
                    image = image.content
                    image = (Image.open(BytesIO(image)))
                    return {'state': True, 'output': image}

                else:
                     return {'state': False, 'output': 'could not find related image'}
            except Exception as e:
                return {'state': False, 'output': 'finding image failed {}'.format(str(e))}

    def upload(self, data):
        try:
            self.data = data
            name = data['image_name'] + ".jpg"
            data['image_name'] = name
            container = data['path_name'].split("/")[0]
            blob = "/".join(data['path_name'].split("/")[1:])
            existance = self.block_blob_service.exists(container)
            byteImgIO = BytesIO()
            byteImg = data['image_file']
            byteImg.save(byteImgIO, "Jpeg")
            byteImgIO.seek(0)
            byteImg = byteImgIO.read()
            dataBytesIO = BytesIO(byteImg)
            if existance is False:
                self.block_blob_service.create_container(container)
                self.block_blob_service.set_container_acl(container, public_access=PublicAccess.Container)
            self.block_blob_service.create_blob_from_stream(container, blob +'/'+ name,
                                                            dataBytesIO,
                                                       content_settings=
                                                            ContentSettings(content_type="image/jpg"))
            return {'state': True, 'output': data['image_name'] + 'uploaded'}

        except Exception as e:
            parse = self.data['image_name'][-6:][:2]
            if parse == '_S':
                return {'state': False, 'output': 'small size failed to upload {}'.format(str(e))}

            elif parse == '_T':
                return {'state': False, 'output': 'thumbnail failed to upload {}'.format(str(e))}
            else:
                return {'state': False, 'output': 'original size failed to upload {}'.format(str(e))}
